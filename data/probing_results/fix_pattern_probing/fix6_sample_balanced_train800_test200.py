# coding: utf-8
# Copyright(c) 2009 - present CNRS
# All rights reserved.

"""
sampling balanced train (800 sentences) & test (200 sentences) of fix6 pattern sentences
for representation-extraction procedure
train: balanced sing & plur , but no attractor-based constraint
test: balanced in terms of sing & plur and whether the embedded noun is attractor or not

"""

import sys
import pandas as pd
import random

# Experiments in ACL2022-how distributed paper used 3 random seeds: 0, 20, 42
file_name = sys.argv[1] # the valid fix6 pattern sentences
out_dir = sys.argv[2]

tab = pd.read_csv(file_name,sep="\t")
sing = tab[tab['correct_number']=="sing"]

sing_ids = sing["constr_id"].tolist()
plur = tab[tab['correct_number']=="plur"]
plur_ids = plur["constr_id"].tolist()

sing_attr = sing[sing['cls_noun_num']!=sing['correct_number']]
sing_no_attr = sing[sing['cls_noun_num']==sing['correct_number']]
# sing items: 50 with attractor and 50 without
test_sing_attr = sing_attr.sample(n=50,random_state=20)
test_sing_no_attr = sing_no_attr.sample(n=50,random_state=20)
test_sing = pd.concat([test_sing_attr,test_sing_no_attr])
# plur items: 50 with attractor and 50 without
plur_attr = plur[plur['cls_noun_num']!=plur['correct_number']] #550
plur_no_attr = plur[plur['cls_noun_num']==plur['correct_number']]
test_plur_attr = plur_attr.sample(n=50,random_state=20)
test_plur_no_attr = plur_no_attr.sample(n=50,random_state=20)
test_plur = pd.concat([test_plur_attr,test_plur_no_attr])
# train: 400 sing items and 400 plur items
train_sing_candidate_ids = [i for i in sing_ids if i not in test_sing["constr_id"].tolist()]
train_plur_candidate_ids = [i for i in plur_ids if i not in test_plur["constr_id"].tolist()]
train_sing_candidate = sing[sing["constr_id"].isin(train_sing_candidate_ids)]
train_plur_candidate = plur[plur["constr_id"].isin(train_plur_candidate_ids)]
train_sing = train_sing_candidate.sample(n=400,random_state=20)
train_plur = train_plur_candidate.sample(n=400,random_state=20)

train = pd.concat([train_sing,train_plur])
test_attr = pd.concat([test_sing_attr,test_plur_attr])
test_no_attr = pd.concat([test_sing_no_attr,test_plur_no_attr])

test_attr.to_csv(out_dir + "test_attr.tab", sep="\t", index=False)
test_no_attr.to_csv(out_dir + "test_no_attr.tab", sep="\t", index=False)
train.to_csv(out_dir + "train.tab", sep="\t", index=False)
with open(out_dir + "test_attr.txt","w") as txt:
    txt.write("\n".join(test_attr['sent'].tolist()))
with open(out_dir + "test_no_attr.txt","w") as txt:
    txt.write("\n".join(test_no_attr['sent'].tolist()))
with open(out_dir + "train.txt","w") as txt:
    txt.write("\n".join(train['sent'].tolist()))
