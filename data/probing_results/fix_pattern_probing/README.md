This folder contains data for fixed pattern probing experiments.

fixed six-word context: `Antecedent ADP NOUN que PRON AUX Past-participle` as in example: "... bureaux en bois qu'il as trouvés ...".

`fix_ante-adp-noun.*` files containing all the valid fixe6 pattern sentences(1940) extracted from the Gutenberg corpora. 

The probing exepriments used the three balanced train/test splits in the corresponding folders each contains 800 training sentences and 200 testing sentences.