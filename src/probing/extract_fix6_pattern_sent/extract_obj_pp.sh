#!/usr/bin/env bash

treebank="/home/bli/TACL/data/agreement/French/obj_pp"
paradigms="/home/bli/synStructure/data/treebanks/French/fr_subj_V_full_paradigms_min1.txt"
vocab="/home/bli/TACL/src/jean-zay-lm/fr-TM/28.2/tokcodes"
trainDIR="/home/bli/synStructure/data/lm_data_prep/data/wiki/French"
#outPref="/home/bli/synStructure/data/agreement/French/subj-verb/subj_relv_verb/fix5_subj-que-v_aux"
outPref="/home/bli/TACL/data/probing_repre/obj_pp_fix5/fix5_ante-adp-noun"

python extract_obj-pp_fix5.py  --treebank $treebank/gutenberg_treebank-67446.conllu \
                             --vocab $vocab \
                             --paradigms $paradigms \
                             --lm_data $trainDIR \
                             --output $outPref



: '
for file in $treebank/*.spacy-conll
do
  #prefix=${file%.spacy-conll} # remove .conll from the right
  #echo $file
  python extract_subj_V_agreement.py  --treebank $file \
                               --vocab $vocab \
                               --paradigms $paradigms

done;
'