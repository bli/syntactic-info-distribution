# -*- coding: utf-8 -*-

""" extract from gutenberg 67 446 conll file a .tab and a .txt file for
 fix pattern with 5 tokens between antecedent and pp:
 antecedent ADP NOUN que PRON AUX target-pp
 
 Remark:
 - It does not require that the alt form of antec and pp exist or in vocab
 """

import depTree
import json
import argparse
import pandas as pd
from collections import Counter,defaultdict
from agreement_utils import match_features,ltm_to_word,get_alt_form,read_paradigms, load_vocab, vocab_freqs, is_good_form

def inside(tree, a):
    # return tuple (nodes, l, r), nodes is the context, l is the cue and r is the target
    if a.child.index < a.head.index:
        nodes = tree.nodes[a.child.index: a.head.index - 1]
        l = a.child
        r = a.head
    else:
        nodes = tree.nodes[a.head.index: a.child.index - 1]
        l = a.head
        r = a.child
    return nodes, l, r

def plurality(morph):

    if "Number=Plur" in morph:
        return "plur"
    elif "Number=Sing" in morph:
        return "sing"
    else:
        return "none"


def extract_sent_features(t,nodes,l,r,vocab):
    """ Extracting some features of the construction and the sentence for data analysis """
    # mirror mode is for generating mirror number sentence for obj_pp french dataset
    r_idx = int(r.index)
    l_idx = int(l.index)
    #s_lm = " ".join([n.word if n.word in vocab else "<unk>" for n in t.nodes[:r_idx-1]])  # prefix
    # problem of numbers : '3 500' is one node in conll, but two tokens for lm
    prefix = " ".join(n.word.replace(" ","") for n in t.nodes[:r_idx-1])
    #s_lm = " ".join(w if w in vocab else "<unk>" for w in prefix.split())
    prefix_lm_list = prefix.split()
    n_unk = len([w for w in prefix_lm_list if w not in vocab])
    correct_number = plurality(r.morph)

    #compute the cls noun number
    context_noun_num = [plurality(n.morph) for n in nodes if
                        n.pos in ["NOUN", "PROPN"] and plurality(n.morph) != "none"]
    cls_noun_num = context_noun_num[-1] if context_noun_num else "none"

    #compute the cls number on the left of target verb
    context_num = [plurality(n.morph) for n in nodes if plurality(n.morph) != "none"]
    cls_token_num = context_num[-1] if context_num else "none"

    # calculate the major number before the target verb in the sentence
    prefix_number_values = [plurality(n.morph) for n in t.nodes[:r.index-1]]
    prefix_number = [v for v in prefix_number_values if v!="none"]
    prefix_number.reverse()# if #sing == #plur, take the closer number to target verb
    com_num = Counter(prefix_number).most_common(1)[0][0]

    # compute the first noun number
    noun_nodes = [n for n in t.nodes[:l.index-1] if n.pos in ["NOUN","PROPN"]]
    noun_num = [plurality(n.morph) for n in noun_nodes if plurality(n.morph) != "none"]
    fst_noun_num = noun_num[0] if noun_num else "none"


    len_prefix = len(prefix.split()) # the number 23 000 is split into 2 tokens, while in conllu, it's a sole node
    len_context = r_idx - l_idx  # n_tokens of context= r_idx - l_idx -1
    return correct_number,cls_noun_num,cls_token_num,fst_noun_num,com_num,prefix,len_prefix,len_context


def features(morph, feature_list):
    '''
    :param morph: conllu features string
    :param feature_list: ex,['Number']
    :return: morph string or 'Number'
    '''
    if not feature_list:
        return morph

    all_feats = morph.split("|")
    feat_values = tuple(f for f in all_feats if f.split("=")[0] in feature_list)

    return "|".join(feat_values)

def match_obj_pp_agreement(nodes,l,r,paradigms,vocab):
    """
    verify if satisfy object participle verb agreement condition in French
    :param nodes: context nodes
    :param l: cue
    :param r: target
    :return: bool
    """
    if r.pos != "VERB":
        return False

    r_child = [n for n in nodes if n.head_id == r.index]

    if not r_child:
        return False

    else:
        # we don't consider the case :
        # - pron antéposé accord pron_pp, e.g. Les échelons qui les ont amenés  (les a "obj" dep_label)
        # - se* aux pp* e.g. façon dont la Ville* de Paris s'* est financièrement engagée* (s' a "obj" dep_label)
        # discard "PRON-VERB" : ce qu'ils ont fait
        # discard "NOUN-AUX" : projet qu' Alain Denvers n' avait pas voulu
        # dans certains corpus, "que" obj ne dispose pas de feats (la plupart ont PronType=Rel)
        pron_object = [n for n in r_child if n.dep_label == "obj" and n.word in ["que","qu'"]]#"Rel" in n.morph]
        r_child_lemma = [n.lemma for n in r_child]
        if not pron_object or "avoir" not in r_child_lemma:
            return False
        else:
            que_id = pron_object[-1].index
            if len(pron_object)>1:
                breakpoint()
            l_feats = features(l.morph, ["Gender", "Number"])
            r_feats = features(r.morph, ["Gender", "Number"])
            if l_feats and r_feats and l_feats == r_feats and len(l_feats.split("|")) == 2 :
                # all the words between cue and target (included) should be in vocabulary :
                in_vocab = all([n.word in vocab for n in nodes + [l, r]])
                #ltm_paradigms = ltm_to_word(paradigms)
                #l_good_form = is_good_form(l.word, l.morph, l.lemma, l.pos, vocab, ltm_paradigms)

                #if not in_vocab or not l_good_form:
                if not in_vocab:
                    return False
            else:
                return False
    return que_id


collective_words = ["plupart","moitié","bande", "dizaine", "ensemble", "foule", "horde", "millier",
                    "multitude", "majorité", "nombre", "ribambelle", "totalité", "troupeau"]

def collect_agreement(trees,  paradigms, vocab):
    output = []
    whole_constr = set()
    constr_id = 0
    ltm_paradigms = ltm_to_word(paradigms)  # best_paradigms_lemmas['be']['Aux'][morph]=='is'
    #dico_context_pos = defaultdict(int)
    for tree in trees:
        for a in tree.arcs:
            if a.length() > 2:
                context_nodes, l, r = inside(tree, a)
                if l.pos not in ["NOUN"] or r.pos != "VERB" or a.dep_label != "acl:relcl":#, "PROPN"]: nominal subject
                    continue
                # make sure there is no "conjunction nominal subject"
                conj_pos = [n.pos for n in context_nodes if n.head_id == l.index and n.dep_label == "conj"]
                if "NOUN" in conj_pos or "PROPN" in conj_pos :
                    continue
                que_index = match_obj_pp_agreement(context_nodes,l,r,paradigms,vocab)
                if a.dep_label=="acl:relcl" and que_index:
                    pattern = "obj_aux_PP"
                    constr = " ".join(str(n.word) for n in tree.nodes[l.index - 1:r.index])
                    if constr in whole_constr:
                        print(constr_id, constr)
                        continue
                    else:
                        whole_constr.add(constr)

                else:
                    continue


                # we don't consider the word "plupart" as subject
                # # toujours annoté singulier, mais verbe s’accorde avec le dernier substantif
                # la plupart des hommes se souviennent … la plupart du monde ne se soucie
                #if l.word.lower() in collective_words:
                #    continue

                if l.word not in vocab or r.word not in vocab:
                    continue

                context_poses = [n.pos for n in context_nodes]
                if context_poses == ["ADP","NOUN","PRON","PRON", "AUX"]:
                    sent = " ".join([n.word.replace(" ", "") for n in tree.nodes])
                    poses = " ".join([n.pos for n in tree.nodes])
                    correct_number, cls_noun_num, cls_token_num, fst_noun_num, com_num, prefix, len_prefix, len_context = extract_sent_features(tree,context_nodes,l,r,vocab)

                else:
                    #if len(context_poses)==5:

                    continue


                form = r.word

                output.append((pattern, constr_id, form, correct_number,cls_noun_num, cls_token_num, fst_noun_num,com_num,prefix,
                               len_prefix, len_context, que_index, sent+" <eos>",poses))
                constr_id += 1

    return output




def main():
    parser = argparse.ArgumentParser(description='Generating sentences based on patterns')

    parser.add_argument('--treebank', type=str, required=True, help='input file (in a CONLL column format)')
    parser.add_argument('--paradigms', type=str, required=True,
                        help="the dictionary of tokens and their morphological annotations")
    parser.add_argument('--vocab', type=str, required=True, help='(LM) Vocabulary to generate words from')
    parser.add_argument('--lm_data', type=str, required=False, help="path to LM data to estimate word frequencies")
    parser.add_argument('--output', type=str, required=True, help="prefix for generated text and annotation data")
 
    args = parser.parse_args()

    print("* Loading trees")
    # trees is a list of DependencyTree objets
    trees = depTree.load_trees_from_conll(args.treebank)
    print("# {0} parsed trees loaded".format(len(trees)))
    # needed for original UD treebanks (e.g. French) which contain spans, e.g. 10-11
    # annotating mutlimorphemic words as several nodes in the tree
    # for t in trees:
    #    t.remerge_segmented_morphemes()
    paradigms = read_paradigms(args.paradigms) #d["is"] == ('be','AUX','Number=Sing|Mood=Ind|Tense=Pres|VerbForm=Fin',100)
    #print(paradigms["fait"])
    vocab = load_vocab(args.vocab)
    data = collect_agreement(trees,paradigms,vocab)

    print("# In total, {0} agreement sentences collected".format(int(len(data))))

    data = pd.DataFrame(data, columns=["pattern","constr_id", "form", "correct_number","cls_noun_num","cls_token_num","fst_noun_num","com_num",
                                   "prefix", "len_prefix", "len_context", "que_index","sent","pos"])

    if args.lm_data:
        freq_dict = vocab_freqs(args.lm_data + "/train.txt", vocab)
        data["freq"] = data["form"].map(freq_dict)
        fields = ["pattern","constr_id", "form", "correct_number","cls_noun_num","cls_token_num","fst_noun_num","com_num",
                                   "prefix", "len_prefix", "len_context", "que_index","freq","sent","pos"]


    else:
        fields = ["pattern","constr_id", "form", "correct_number","cls_noun_num","cls_token_num","fst_noun_num","com_num",
                                   "prefix", "len_prefix", "len_context", "que_index","sent","pos"]

    data[fields].to_csv(args.output  + ".tab", sep="\t", index=False)
    with open(args.output + ".txt","w") as txt:
        txt.write("\n".join(data['sent'].tolist()))



if __name__ == "__main__":
    main()
