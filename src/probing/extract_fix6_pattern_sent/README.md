# Extract fix6_obj-pp sentences and stats for representation-extraction
> pattern: `antecedent ADP NOUN que PRON AUX target-pp`

1.execute script `extract_obj_pp.sh` --> 1940 sentences(i.e. `fix6_antec-adp-noun.tab/txt`) for sampling a balanced probing test set stored in `distribution_syntactic_info/data/probing_context`

2.run `distribution_syntactic_info/data/probing_context/sample_balanced_fix6.py` to sample with 3 random seeds balanced fix6 sentences for representation extraction procedure

3.run `Lm4ling/extract_repre.sh` script (obj_pp_fix6_pattern_representation.py`) to extract the corresponding tokens representations.   