# -*- coding: utf-8 -*-

"probing classifier with grid search for C values train and test on prefix, context, suffix "

import sys
import bloscpack as bp
import numpy as np
from sklearn.metrics import accuracy_score,classification_report
from sklearn.model_selection import GridSearchCV as gsc
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.utils import shuffle
from sklearn.preprocessing import StandardScaler

x_correct_name = sys.argv[1]
y_correct_name = sys.argv[2]
x_wrong_name = sys.argv[3]
y_wrong_name = sys.argv[4]
pos = sys.argv[5]

x_correct_data = bp.unpack_ndarray_from_file(x_correct_name)
y_correct_data = bp.unpack_ndarray_from_file(y_correct_name)
x_wrong_data = bp.unpack_ndarray_from_file(x_wrong_name)
y_wrong_data = bp.unpack_ndarray_from_file(y_wrong_name)

x_correct_train, x_correct_test, y_correct_train, y_correct_test = train_test_split(x_correct_shuffled,y_correct_shuffled, test_size=0.2, random_state=42)
x_wrong_train, x_wrong_test, y_wrong_train, y_wrong_test = train_test_split(x_wrong_shuffled,y_wrong_shuffled, test_size=0.2, random_state=42)

# training items do not have correct/wrong constraints
X_train = np.vstack((x_correct_train, x_wrong_train))
Y_train = np.concatenate((y_correct_train, y_wrong_train))

# scaling
scaler = StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
x_correct_test = scaler.transform(x_correct_test)
x_wrong_test = scaler.transform(x_wrong_test)

X_shuffled, Y_shuffled = shuffle(X_train, Y_train, random_state=42)

# imbalanced data with 65% singular, so add parameter "class_weight"
model = LogisticRegression(class_weight = 'balanced',random_state=42,max_iter=1000)

# defining hyper-parameter range
params = {"C": [0.00001,0.0001,0.001,0.01,0.02,0.03,0.04,0.05,0.1,1]}
repr_clf = gsc(model,params,n_jobs=1,refit = True)

repr_clf.fit(X_shuffled,Y_shuffled)
print(repr_clf.best_estimator_)

# do prediction on test set
y_correct_pred= repr_clf.predict(x_correct_test)
print("correct predictions probing: ",accuracy_score(y_correct_test,y_correct_pred))

y_wrong_pred= repr_clf.predict(x_wrong_test)
print("wrong predictions probing ",accuracy_score(y_wrong_test,y_wrong_pred))

