#!/usr/bin/env bash

dir="../../data/French_obj-pp_agreement"
masking="../../data/masking_intervention"
for file in $masking/*.json
do
	prefix=${file%.json} # remove _X.blp from the right
  python nnlm.py ../Lm4ling/28_2/  --test_file $dir/original_68497.txt \
                            --target_file $dir/original_68497.tab \
                            --json_file $file \
                            --out ${prefix}_transformer_28_2.pred
done;