# Localization of syntactic information in long-distance agreement tasks
This repository contains [`data`](data) and code [`src`](src)for a reproduction of the experiments of the following paper: [How Distributed are Distributed Representations? An Observation
  on the Locality of Syntactic Information in Verb Agreement Tasks](https://aclanthology.org/2022.acl-short.54/) 

If you use the resources in the repository, please cite the paper:
```
@inproceedings{li-etal-2022-distributed,
    title = "How Distributed are Distributed Representations? An Observation on the Locality of Syntactic Information in Verb Agreement Tasks",
    author = "Li, Bingzhi  and
      Wisniewski, Guillaume  and
      Crabb{\'e}, Benoit",
    booktitle = "Proceedings of the 60th Annual Meeting of the Association for Computational Linguistics (Volume 2: Short Papers)",
    month = may,
    year = "2022",
    address = "Dublin, Ireland",
    publisher = "Association for Computational Linguistics",
    url = "https://aclanthology.org/2022.acl-short.54",
    doi = "10.18653/v1/2022.acl-short.54",
    pages = "501--507"
}
```